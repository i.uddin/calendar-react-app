const express = require('express');
const sqlite = require('sql.js');
const fs = require('fs');

const app = express();
const fileBuffer = fs.readFileSync('./db/calendar.sqlite');
const db = new sqlite.Database(fileBuffer);


app.set('port', process.env.PORT || 3001);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build'));
}

app.get('/:year/:month/:day', (req, res) => {
    const year = req.params.year;
    const month = req.params.month;
    const day = req.params.day;

    const rows = db.exec(
        `
        SELECT * FROM calendar 
        WHERE datetime == date('${year}-${month}-${day}')
        `
    );
    
    if (rows.length > 0) {
        const columnNames = rows[0].columns;
        const events = rows[0].values.map(event => {
            return event.map((entry, i) => [columnNames[i], entry])
        });
        const eventsJson = events.map(event => {
            let obj = {};
            event.forEach(entry => obj[entry[0]] = entry[1]);
            return obj;
        });
        res.send(eventsJson);
    } else {
        res.send([]);
    }
});

app.listen(app.get('port'), () => {
    console.log(`Server listening at http://localhost:${app.get('port')}/`);
});
