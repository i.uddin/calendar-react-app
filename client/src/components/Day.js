import React, { Component } from 'react'

export default class Day extends Component {
    getStyle = () => {
        if (this.props.selectedDate.date() === this.props.day) {
            return 'selected';
        } else {
            return null;
        }
    }


    render() {
        const day = this.props.day;
        return (
            <div 
                onClick={this.props.selectDay.bind(this, day)} 
                className={"day " + this.getStyle()}
            >
                <h3>
                    {this.props.day}
                </h3>
            </div>
        )
    }
}
