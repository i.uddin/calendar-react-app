import React, { Component } from 'react';
import Day from './Day';
import uuidv4 from 'uuid/v4';

export default class CalenderDays extends Component {

  render() {
    return this.props.days.map(day => (
      <Day
        key={uuidv4()}
        day={day}
        selectDay={this.props.selectDay}
        selectedDate={this.props.selectedDate}
      />
    ))
  }
}
