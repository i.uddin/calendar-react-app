import React from 'react'

export default function DayLabels() {
  return (
    <div className="calendarLabel">
        <div className="label">Mon</div>
        <div className="label">Tue</div>
        <div className="label">Wed</div>
        <div className="label">Thu</div>
        <div className="label">Fri</div>
        <div className="label">Sat</div>
        <div className="label">Sun</div>
    </div>
  )
}
