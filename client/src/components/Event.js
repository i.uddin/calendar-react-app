import React, { Component } from 'react'
import moment from 'moment';

export default class Event extends Component {
  render() {
    const startTime = moment(this.props.startTime).format('HH:mm');
    const endTime = moment(this.props.endTime).format('HH:mm');
    
    return (
      <div className="event">
        <h4>{startTime} - {endTime}</h4>
        <h2>{this.props.name}</h2>
      </div>
    )
  }
}
