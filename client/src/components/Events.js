import React, { Component } from 'react';
import Event from './Event';


export default class Events extends Component {
  render() {
    if (this.props.events.length > 0) {
        return this.props.events.map(event => (
            <Event
                key={event.id}
                name={event.event_name}
                startTime={event.start_time}
                endTime={event.end_time}
                location={event.location}
                category={event.category}
                color={event.color}
            />
        ));
    } else {
        return (
            <h2>No events for today!</h2>
        )
    }
  }
}
