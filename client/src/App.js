import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import CalendarDays from './components/CalenderDays';
import DayLabels from './components/layout/DayLabels'
import moment from 'moment';
import axios from 'axios';
import Events from './components/Events';

class App extends Component {
  state = {
    date: moment(),
    events: []
  }

  dayOfFirstDayOfMonth() {
    const currentDate = moment([
      this.state.date.year(),
      this.state.date.month(),
      1
    ]);
    return currentDate.day()
  }

  getDaysOfMonth() {
    const currentMonth = this.state.date.month();
    const nOfDays = moment().month(currentMonth).daysInMonth();
    const dayOffset = this.dayOfFirstDayOfMonth() + 1;
    const days = [];

    for (let i = 1; i < nOfDays + dayOffset - 1; i++) {
      if (i + 1 < dayOffset) {
        days.push(null)
      } else {
        days.push(i - dayOffset + 2)
      }      
    }
    return days;
  }

  changeMonth(direction) {
    if (direction === -1) {
      this.setState({
        date: this.state.date.subtract(1, 'months')
      })
      this.getEventsForDay();
    } else if (direction === 1) {
      this.setState({
        date: this.state.date.add(1, 'months')
      })
      this.getEventsForDay();
    }
  }

  selectDay = (day) => {
    if (day > 0) {
      this.setState({
        date: this.state.date.date(day)
      })
      this.getEventsForDay();
    }
  }

  getEventsForDay = () => {
    const year = this.state.date.format('YYYY');
    const month = this.state.date.format('MM');
    const day = this.state.date.format('DD');
    axios.get(`/${year}/${month}/${day}`)
      .then(res => {
        this.setState({
          events: res.data
        })
      });
  }

  componentDidMount() {
    this.getEventsForDay();
  }

  render() {
    return (
        <Router>
          <div className="CalendarApp">
            <h1>calendar</h1>
              <Route exact path="/" render={props => (
                <React.Fragment>
                  <div className="PageHolder">
                    <div className="CalendarPage">
                      <div className="header">
                        <div onClick={() => {this.changeMonth(-1)}} className="selector left">
                          <img src="/imgs/back.svg" alt="prev" className="monthSelector"/>
                        </div>
                        <div className="month">
                          <h1>
                            {this.state.date.format('MMMM YY')}
                          </h1>
                        </div>
                        <div onClick={() => {this.changeMonth(1)}} className="selector right"> 
                          <img src="/imgs/next.svg" alt="prev" className="monthSelector"/>
                        </div>
                      </div>
                      <DayLabels/>
                      <div className="calendarDays">
                        <CalendarDays
                          days={this.getDaysOfMonth()}
                          selectDay={this.selectDay}
                          selectedDate={this.state.date}
                        />
                      </div>
                    </div>
                    <div className="UnderPage"></div>
                    <div className="UnderPage"></div>
                  </div>
                  <div className="Events">
                    <Events
                      events={this.state.events}
                    />
                  </div>
                </React.Fragment>
              )}>
              </Route>
          </div>
        </Router>
    );
  }
}

export default App;
